This is probably a little over-the top. I did spend more than 8 hours on this, but a lot of that time was learning the SQLite framework that I had wanted to use in the past, so I decided to take advantage of this opportunity to learn it.

This project has unit tests for almost all the low-level service components. If this was a real project, it would be more thoroughly unit tested.

It is heavily factored into multiple components and the database keeps an audit trail of how and when creadentials were created, which provides options for future maintainers of the software.

I don't over engineer everything to this level. It seems like an interesting way to do services that are subject to technological change and a multiple threats. It attempts, at least, to provide tools to maintainers to maximize security while minimizing user disruption.

It uses a home-grown randomly salted hashed password store. A real system would use something approved by OWASP and/or other organizations, but I did not want to use one of those for a few reasons: I did not know if you wanted to test my understanding of the principles and those frameworks are complex and present the risk of intensional or unintentional vulnerabilities and would, in the real world, have to be vetted thoroughly.

Thanks for this oppotunity.