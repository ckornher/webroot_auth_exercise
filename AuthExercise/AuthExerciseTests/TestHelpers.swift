//
//  TestHelpers.swift
//  AuthExerciseTests
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import XCTest
@testable import AuthExercise

func deleteStoreFile(store: SQLiteAuthStore) throws {
    let fileURL = URL(fileURLWithPath: store.dbFilepath)
    do {
        try FileManager.default.removeItem(at:fileURL)
        NSLog("DB File deleted: \(String(describing:fileURL))")
    } catch {
        NSLog("Error deleting file: \(String(describing:fileURL))")
    }
}
