//
//  EmailValidatorTests.swift
//  AuthExerciseTests
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import XCTest

@testable import AuthExercise

class EmailValidatorTests: XCTestCase {

    let validator = EmailValidator()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidEmails() throws {
        XCTAssertNoThrow(try validator.validate(username: "A@b.com"))
    }
    
    func testInvalidEmails() throws {
        XCTAssertThrowsError(try validator.validate(username: "A")) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.usernameInvalid)
            XCTAssertEqual(message, "You must enter a valid email")
        }
    }
}
