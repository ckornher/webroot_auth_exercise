//
//  HomegrownAuthCryptoTests.swift
//  AuthExerciseTests
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import XCTest

@testable import AuthExercise

/// Note that password length is controlled by a different component
class HomegrownAuthCryptoTests: XCTestCase {
    let crypto = HomegrownAuthCrypto()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHashAndAuthGoodPassword() throws {
        let password = "This is a password"
        let (salt, hash) = try crypto.hashPassword(password)
        
        XCTAssertNotNil(salt)
        XCTAssertNotNil(hash)
        
        try crypto.authenticate(username: "NotUsed", password: password, salt: salt, hash: hash)
    }
    
    func testHashAndAuthBadPassword() throws {
        let password = "A Different Password"
        let (salt, hash) = try crypto.hashPassword(password)
        
        XCTAssertNotNil(salt)
        XCTAssertNotNil(hash)
        
        XCTAssertThrowsError(try crypto.authenticate(username: "NotUsed", password: "bad", salt: salt, hash: hash)) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.authFailed)
            XCTAssertEqual(message, "No matching username/password found")
        }
    }
}
