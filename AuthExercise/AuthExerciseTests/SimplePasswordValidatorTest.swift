//
//  SimplePasswordValidatorTest.swift
//  AuthExerciseTests
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import XCTest
@testable import AuthExercise

class SimplePasswordValidatorTest: XCTestCase {
    
    let validator = SimplePasswordValidator()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLength() throws {
        XCTAssertNoThrow(try validator.validate(password: "aA345678"))
        XCTAssertNoThrow(try validator.validate(password: "aA345678901234567890"))
        
        XCTAssertThrowsError(try validator.validate(password: "aA34567")) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.passwordNotStrongEnough)
            XCTAssertEqual(message, "Password must be 8 or more characters long")
        }
    }
    
    func testUppercase() throws {
        XCTAssertThrowsError(try validator.validate(password: "a2345678")) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.passwordNotStrongEnough)
            XCTAssertEqual(message, "Password must contain at least one uppercase letter")
        }
    }
    
    func testLowercase() throws {
        XCTAssertThrowsError(try validator.validate(password: "A2345678")) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.passwordNotStrongEnough)
            XCTAssertEqual(message, "Password must contain at least one lowercase letter")
        }
    }
    
    func testNumber() throws {
        XCTAssertThrowsError(try validator.validate(password: "aaaaAAAA")) { error in
            XCTAssertNotNil(error as? AuthError)
            guard case AuthError.userError( let type, let message) = error else {
                return XCTFail()
            }
            
            XCTAssertEqual(type, AuthIssueType.passwordNotStrongEnough)
            XCTAssertEqual(message, "Password must contain at least one number")
        }
    }
}
