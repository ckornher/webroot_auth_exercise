//
//  SQLiteAuthStoreTests.swift
//  AuthExerciseTests
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import XCTest
@testable import AuthExercise

class SQLiteAuthStoreTests: XCTestCase {
    
    let store = SQLiteAuthStore(fileName: "TestDB.sqlite3")

    override func setUp() {
        do {
            try deleteStoreFile(store: store)
            try store.openAndIntitialize()
        } catch {
            NSLog("Error Setting up")
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        // Do not delete the test DB so that it can be examined
    }

    func testCredentialSequence() throws {
        try store.storeCredentials(username: "User",
                                   salt: "salt1",
                                   hash: "hash1",
                                   .email_v1, .v1, .v1)
        
        let credential = try store.currentCredential(username: "User")
        XCTAssertNotNil(credential)
        if let c = credential {
            XCTAssertEqual(c.username, "User")
            XCTAssertEqual(c.salt, "salt1")
            XCTAssertEqual(c.hash, "hash1")
            XCTAssert(c.isActive)
            XCTAssert(Date().timeIntervalSince(c.addedDate) < 10.0)
            XCTAssertEqual(c.userValidationVersion, AuthServiceVersion.Username.email_v1.rawValue)
            XCTAssertEqual(c.passwordValidationVersion, AuthServiceVersion.Password.v1.rawValue)
            XCTAssertEqual(c.cryptoVersion, AuthServiceVersion.Crypto.v1.rawValue)
        }
        
        // Add a second credential
        try store.storeCredentials(username: "User",
                                   salt: "salt2",
                                   hash: "hash2",
                                   .email_v1, .v1, .v1)

        let credential2 = try store.currentCredential(username: "User")
        XCTAssertNotNil(credential2)
        if let c = credential2 {
            XCTAssertEqual(c.username, "User")
            XCTAssertEqual(c.salt, "salt2")
            XCTAssertEqual(c.hash, "hash2")
            XCTAssert(c.isActive)
            XCTAssert(Date().timeIntervalSince(c.addedDate) < 10.0)
            XCTAssertEqual(c.userValidationVersion, AuthServiceVersion.Username.email_v1.rawValue)
            XCTAssertEqual(c.passwordValidationVersion, AuthServiceVersion.Password.v1.rawValue)
            XCTAssertEqual(c.cryptoVersion, AuthServiceVersion.Crypto.v1.rawValue)
        }
        
        // Add a credential for a different user
        try store.storeCredentials(username: "AnotherUser",
                                   salt: "salt3",
                                   hash: "hash3",
                                   .email_v1, .v1, .v1)
        
        let credential3 = try store.currentCredential(username: "AnotherUser")
        XCTAssertNotNil(credential3)
        if let c = credential3 {
            XCTAssertEqual(c.username, "AnotherUser")
            XCTAssertEqual(c.salt, "salt3")
            XCTAssertEqual(c.hash, "hash3")
            XCTAssert(c.isActive)
            XCTAssert(Date().timeIntervalSince(c.addedDate) < 10.0)
            XCTAssertEqual(c.userValidationVersion, AuthServiceVersion.Username.email_v1.rawValue)
            XCTAssertEqual(c.passwordValidationVersion, AuthServiceVersion.Password.v1.rawValue)
            XCTAssertEqual(c.cryptoVersion, AuthServiceVersion.Crypto.v1.rawValue)
        }
        
        XCTAssertEqual(try store.credentialHistory(username: "User").count, 2)
        XCTAssertEqual(try store.credentialHistory(username: "AnotherUser").count, 1)
    }
}
