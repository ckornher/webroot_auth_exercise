//
//  AuthExerciseTabViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class AuthExerciseTabViewController: NSTabViewController {
    
    enum Tab : String {
        case login
        case loggedIn
        case register
        case changePassword
    }
    
    /// The user authorization state
    let userAuthorization = UserAuthorization()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userAuthorization.delegate = self
        
        for item in tabViewItems {
            guard let baseAuthView = item.viewController as? BaseAuthViewController else {
                preconditionFailure("not a BaseAuthViewController")
            }
            baseAuthView.userAuthorization = userAuthorization
        }
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        
    }
   // MARK: NSTabViewDelegate

    /// Allow the UI to show tabs, depending upon authorized state.
    override func tabView(_ tabView: NSTabView, shouldSelect tabViewItem: NSTabViewItem?) -> Bool {
        guard let identiferStr = tabViewItem?.identifier as? String else {
            return false // Must have a tab selected
        }
        
        guard let tab = Tab(rawValue: identiferStr) else {
            assert(false, "Tabs have missing or incorrect identifier:\(String(describing:identiferStr))")
        }
        
        guard tabView.selectedTabViewItem != nil else {
            return true
        }
        
        switch (userAuthorization.authStatus, tab) {
        // Authorized
        case (.authorized, .changePassword):
            return true
        
        case (.authorized, .loggedIn):
            return true
            
        case (.authorized, _):
            return false

        // UnAuthorized
        case (.unAuthorized, .login):
            return true
        
        case (.unAuthorized, .register):
            return true
        
        case (.unAuthorized, _):
            return false
        }
        
        // Will never be executed, but docs say that it should be called
        // return super.tabView( tabView, shouldSelect: tabViewItem)
    }
    
    func tabWithIdentifier(_ identifier: Tab) -> NSTabViewItem {
        guard
            let item = tabViewItems.first(where: {$0.identifier as? String == identifier.rawValue })
        else {
            preconditionFailure("Tab not found with identifier:\(identifier.rawValue)")
        }
        return item
    }
}

extension AuthExerciseTabViewController : UserAuthorizationDelegate {
    func authorization(_ authorization: UserAuthorization, actionPerformed action: UserAuthorization.Action) {
        switch(action) {
            
        case .loggedIn:
            if userAuthorization.passwordIsExpired {
                selectTab(.changePassword)
            }
            else {
                selectTab(.loggedIn)
            }

        case .loggedOut:
            selectTab(.login)
            
        case .changedPassword:
            selectTab(.loggedIn)

        case .registered:
            selectTab(.login)
        }
    }
    
    // Utility
    func selectTab(_ tab: Tab) {
        tabView.selectTabViewItem(withIdentifier:tab.rawValue)
    }
}
