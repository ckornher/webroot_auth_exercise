//
//  UserAuthorization.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

// Simplify Error handling for UI
enum UIError : Error {
    case any(_ message: String)
}

extension UIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .any(let message):
            return message
        }
    }
}

protocol UserAuthorizationDelegate : class {
    func authorization( _ authorization: UserAuthorization, actionPerformed: UserAuthorization.Action)
}

/// A Single user's authorization state
class UserAuthorization: NSObject {
    let internalErrorMessage = "An internal error occurred. Contact your system administrator".localized

    enum Status {
        case unAuthorized
        case authorized
    }
    
    enum Action {
        case loggedIn
        case loggedOut
        case changedPassword
        case registered
    }
    
    weak var delegate: UserAuthorizationDelegate?
    
    var authService: AuthService { return AppDelegate.instance.authService }
    
    private(set) var authStatus: Status = .unAuthorized
    private(set) var authedUsername: String?
    private(set) var passwordIsExpired = false
    
    func login(username: String, password: String ) throws {
        do {
            passwordIsExpired = try !authService.authenticate(username: username, password: password)
            authStatus = .authorized
            authedUsername = username
            delegate?.authorization(self, actionPerformed: .loggedIn)
        }
        catch {
            if let error = error as? AuthError {
                authStatus = .unAuthorized
                authedUsername = nil
                
                switch(error)
                {
                case .userError(_, let message):
                    throw UIError.any(message)
                    
                case .internalError( let message ):
                    print( message )
                }
            }

            throw UIError.any(internalErrorMessage)
        }
    }
    
    func register(username: String, password: String, passwordCopy: String ) throws {
        do {
            try authService.register(username: username, password1: password, password2: passwordCopy)
            delegate?.authorization(self, actionPerformed: .registered)
        }
        catch {
            if let error = error as? AuthError {
                authStatus = .unAuthorized
                authedUsername = nil
                
                switch(error)
                {
                case .userError(_, let message):
                    throw UIError.any(message)
                    
                case .internalError( let message ):
                    print( message )
                }
            }
            
            throw UIError.any(internalErrorMessage)
        }
    }
    
    func changePassword(username: String, oldPassword: String, password: String, passwordCopy: String ) throws {
        do {
            try authService.changePassword(username: username,
                                           oldPassword: oldPassword,
                                           newPassword1: password,
                                           newPassword2: passwordCopy)
            delegate?.authorization(self, actionPerformed: .changedPassword)
        }
        catch {
            if let error = error as? AuthError {
                
                switch(error)
                {
                case .userError(let type, let message):
                    if type == .authFailed {
                        throw UIError.any("Old password was not correct")
                    }
                    
                    throw UIError.any(message)
                    
                case .internalError( let message ):
                    print( message )
                }
            }
            
            throw UIError.any(internalErrorMessage)
        }
    }
    
    func logout() {
        authStatus = .unAuthorized
        authedUsername = nil
        delegate?.authorization(self, actionPerformed: .loggedOut)
    }    
}
