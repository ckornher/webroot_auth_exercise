//
//  AppDelegate.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var authService: AuthService = makeSimpleAuthService()
    
    static var instance: AppDelegate { return NSApp.delegate as! AppDelegate }

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        do {
            try authService.start()
        }
        catch {
            print( "Could not start auth service: \(error.localizedDescription)")
            fatalError()
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}

