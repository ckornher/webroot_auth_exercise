//
//  AuthServiceImpl.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

/// Version strings, typed to the thing that they are versioning. These strings are stored with the credential info
/// to support migration to new business rules and new cryptographic algorithms.
enum AuthServiceVersion {
    enum Username : String {
        case email_v1
    }

    enum Password : String {
        case v1
    }

    enum AuthStore : String {
        case v1
    }

    enum Crypto : String {
        case v1
    }

    enum Store : String {
        case v1
    }
}

// MARK: Component Protocols

/// Typed to reduce the chance of transposition errors
protocol UsernameValidator  {
    var userNameValidatorVersion: AuthServiceVersion.Username { get }

    func validate(username: String) throws
}

/// Typed to reduce the chance of transposition errors
protocol PasswordValidator {
    var passwordValidatorVersion: AuthServiceVersion.Password { get }

    var allRulesDescription: String { get }
    
    func validate(password: String) throws
}

/// Cryptographic logic
protocol PasswordCrypto {
    var cryptoVersion: AuthServiceVersion.Crypto { get }
   
    /// - throws: internal errors only (this should never happen, but in case other services are involved, or???
    func hashPassword(_ password: String) throws -> (salt: String, hash: String)
    
    /// - throws: `userError` with type: `authFailed`
    func authenticate( username: String, password: String, salt: String, hash: String) throws
    
    /// Allows history to hash the password against previous salts to check for equality
    func hashMethod(password: String, salt: String) -> String
}

struct CredentialEntry {
    let username: String
    let salt: String
    let hash: String
    let isActive: Bool
    let addedDate: Date
    let userValidationVersion: String
    let passwordValidationVersion: String
    let cryptoVersion: String
}

/// History could be personal history, or it could reflect the history of successful dictionary attacks, etc.
/// This service can expire passwords based upon their age or the versions of the services used to manage them.
/// - note: A version is not required as this leaves no artifacts
protocol CredentialHistoryChecker {
    
    /// Can expire for dates, deprecated/compromised usernames or credentials
    /// - throws: `AuthError.userError` with type: `passwordExpired`
    /// - note: Deprecated usernames not supported in this example
    /// - note: This is not used in this example
    func checkNotExpired(credential: CredentialEntry) throws -> Bool
    
    /// Checks the password history for duplicates
    /// - throws: `AuthError.userError` with type: `passwordRepeted`
    func checkPasswordHistory(username: String, password: String, crypto: PasswordCrypto, store: AuthStore) throws
    
    /// Checks for weakness in new passwords that meet the pattern criteria e.g. common "leet" phrases, and similarity to
    /// the password being replaced.
    /// - throws: `AuthError.userError` with type: `passwordSimilar`, `passwordWeak`
    func checkWeakness(oldPassword: String, newPassword: String) throws
}

/// More-or-less crypto-agnostic storage
protocol AuthStore {
    var storeVersion: AuthServiceVersion.Store { get }
    
    func openAndIntitialize() throws
    
    /// - note: The last 3 arguments cannot be transposed, so names are ommitted
    func storeCredentials(username: String,
               salt: String,
               hash: String,
               _ userValidationVersion: AuthServiceVersion.Username,
               _ passwordValidationVersion: AuthServiceVersion.Password,
               _ cryptoVersion: AuthServiceVersion.Crypto) throws
    
    func currentCredential(username: String) throws -> CredentialEntry?
    
    func credentialHistory(username: String) throws -> [CredentialEntry]
}

// MARK: -

/// ## Architecture
/// The service is factored into components that perform the different, independent tasks. While adding complexity, this
/// simplifies maintenance, allowing each component to be maintained independently. Business rules and best cryptographic
/// practices change over time. Componentizing each aspect of the system simplifies the migration of independent components.
/// "Mix and Matching" to create different kinds of systems is also possible
///
/// This design is overkill for most systems, but this is an exercise, and since this is not a real system with real requirements,
/// I took the liberty of assuming that this system should be desinged to accommodate change, an indefinite lifetime, and to be
/// extensible to support relatively sophisticated and perhaps multiple applications.
///
/// The complexity / functionality tradeoff is something that I would discuss with others before writing code like this.
///
/// While no provision is made in this code for multiple cryptographic algorithms or username types, version information is stored
/// with the user credentials to allow future versions of the system to intelligently migrate credentials to conform to new rules.
///
/// Salts are stored independnetly from hashed passwords in this system, even though some systems store salts in the password hash,
/// e.g. https://www.example-code.com/swift/bcrypt_hash_password.asp
///
/// A real application might want to use a dependency injection lbrary for the "sub-services"
///
/// ## Operation Model
/// The model for all operations is to succeed silently and fail with excpetions thta contain information to inform users and
/// drive sophisticated UIs.

public class AuthServiceImpl {
    let usernameValidator: UsernameValidator
    let passwordValidator: PasswordValidator
    let authStore: AuthStore
    let crypto: PasswordCrypto
    let historyChecker: CredentialHistoryChecker?
    
    /// The most basic concurrency management. Not needed in this UI appliction, as everything occurs on the main queue, but
    /// this will ensure that this class is thread safe. A more sphisticated application would use promises, possibly with Swift NIO
    /// to provide an asynchronous service. The semaphore is compatible with asynchronous wrappers.
    let semaphore = DispatchSemaphore(value: 1)

    init(usernameValidator: UsernameValidator,
         passwordValidator: PasswordValidator,
         authStore: AuthStore,
         crypto: PasswordCrypto,
         historyChecker: CredentialHistoryChecker? = nil) {
        
        semaphore.wait()
        defer { semaphore.signal() }
        
        self.usernameValidator = usernameValidator
        self.passwordValidator = passwordValidator
        self.authStore = authStore
        self.crypto = crypto
        self.historyChecker = historyChecker
    }
    
    
    // Mark: - Private, non-thread safe methods used in multiple public methods
    fileprivate func unsafeAuthenticate(username: String, password: String) throws -> Bool {

        guard let credential = try authStore.currentCredential(username: username) else {
            throw AuthIssueType.authFailed.makeUserError()
        }
        
        try crypto.authenticate( username: username,
                                 password: password,
                                 salt: credential.salt,
                                 hash: credential.hash)
        
        return try historyChecker?.checkNotExpired(credential: credential) ?? true
    }
    
    /// - throws: AuthError.serError with types: newPasswordsDontMatch, passwordNotStrongEnough, passwordRepeted
    fileprivate func unsafeRegister(username: String, password1: String, password2: String) throws {
        try usernameValidator.validate(username: username)
        try passwordValidator.validate(password: password1)
        guard password1 == password2 else {
            throw AuthIssueType.newPasswordsDontMatch.makeUserError()
        }
        
        try historyChecker?.checkPasswordHistory(username: username,
                                                 password: password1,
                                                 crypto: crypto,
                                                 store: authStore)
        
        let (salt, hash) = try crypto.hashPassword(password1)
        try authStore.storeCredentials(username: username,
                                       salt: salt,
                                       hash: hash,
                                       usernameValidator.userNameValidatorVersion,
                                       passwordValidator.passwordValidatorVersion,
                                       crypto.cryptoVersion)
    }
}

/// AuthService Implementation (Thread Safe)
extension AuthServiceImpl : AuthService {
    public var passwordRules: String { return passwordValidator.allRulesDescription }
    
    public func start() throws {
        semaphore.wait()
        defer { semaphore.signal() }
        
        try authStore.openAndIntitialize()
    }
    
    public func register(username: String, password1: String, password2: String) throws {
        semaphore.wait()
        defer { semaphore.signal() }
        
        try unsafeRegister(username: username, password1: password1, password2: password2)
    }
    
    public func authenticate(username: String, password: String) throws -> Bool {
        semaphore.wait()
        defer { semaphore.signal() }
        
        return try unsafeAuthenticate(username: username, password: password)
    }
    
    public func changePassword(username: String, oldPassword: String, newPassword1: String,  newPassword2: String) throws {
        semaphore.wait()
        defer { semaphore.signal() }
        
        let _ = try unsafeAuthenticate(username: username, password: oldPassword)
        try historyChecker?.checkWeakness(oldPassword: oldPassword, newPassword: newPassword1)
        try unsafeRegister(username: username, password1: newPassword1, password2: newPassword2)
    }
}
