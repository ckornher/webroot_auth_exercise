//
//  SQLiteAuthStore.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

import SQLite

class SQLiteAuthStore: AuthStore {
    let fileName: String
    let fileDir: String
    let dbFilepath: String
    
    var db: Connection!
    
    /// SQLite Table 
    struct Users {
        static let table = Table("users")

        static let id = Expression<Int64>("id")
        static let username = Expression<String>("username")
        static let salt = Expression<String>("salt")
        static let hash = Expression<String>("hash")
        static let isActive = Expression<Bool>("is_active")
        static let addedDate = Expression<Date>("added_date")
        static let userValidationVersion = Expression<String>("user_val_ver")
        static let passwordValidationVersion = Expression<String>("pw_val_ver")
        static let cryptoVersion = Expression<String>("crypto_ver")
    }

    init(fileName: String="AuthDB.sqlite3") {
        self.fileName = fileName
        fileDir = NSSearchPathForDirectoriesInDomains(
            .applicationSupportDirectory, .userDomainMask, true
            ).first! + "/" + Bundle.main.bundleIdentifier!
        
        dbFilepath = "\(fileDir)/\(fileName)"
    }

    func openAndIntitialize() throws {
        // Open the database file
        // create parent directory iff it doesn’t exist
        try FileManager.default.createDirectory(
            atPath: fileDir, withIntermediateDirectories: true, attributes: nil
        )
        
        db = try Connection(dbFilepath)
        print("SQLite DB opened at: \(dbFilepath)")
        
        // Create the users table
        try db.run(Users.table.create(ifNotExists: true) { t in
            t.column(Users.id, primaryKey: true) //     "id" INTEGER PRIMARY KEY NOT NULL,
            t.column(Users.username)
            t.column(Users.salt)
            t.column(Users.hash)
            t.column(Users.isActive)
            t.column(Users.addedDate)
            t.column(Users.userValidationVersion)
            t.column(Users.passwordValidationVersion)
            t.column(Users.cryptoVersion)
        })
        
        try db.run(Users.table.createIndex(Users.username, ifNotExists: true))
    }
    
    // MARK: - AuthStore
    var storeVersion = AuthServiceVersion.Store.v1
    
    func storeCredentials(username: String,
                          salt: String,
                          hash: String,
                          _ userValidationVersion: AuthServiceVersion.Username,
                          _ passwordValidationVersion: AuthServiceVersion.Password,
                          _ cryptoVersion: AuthServiceVersion.Crypto) throws {
        try db.transaction {
            // Deactivate all active credentials. There should be only one.
            let activeUserCredentials = Users.table.filter(Users.username == username).filter(Users.isActive)
            try db.run(activeUserCredentials.update(Users.isActive <- false))
            
            // Insert the new credentials
            try db.run(Users.table.insert(Users.username <- username,
                                          Users.salt <- salt,
                                          Users.hash <- hash,
                                          Users.isActive <- true,
                                          Users.addedDate <- Date(),
                                          Users.userValidationVersion <- userValidationVersion.rawValue,
                                          Users.passwordValidationVersion <- passwordValidationVersion.rawValue,
                                          Users.cryptoVersion <- cryptoVersion.rawValue))
        }
    }
    
    func currentCredential(username: String) throws -> CredentialEntry? {
        let userHistory = try credentialHistory(username: username)
        
        // There can be at most one active credential at this point
        let currentCredential = userHistory.first {$0.isActive}
        
        return currentCredential
    }
    
    func credentialHistory(username: String) throws -> [CredentialEntry] {
        let userHistoryRows = try Array(db.prepare(Users.table.filter(Users.username == username).order(Users.addedDate)))
        let userHistory = userHistoryRows.map{ row in
            CredentialEntry( username: row[Users.username],
                             salt: row[Users.salt],
                             hash: row[Users.hash],
                             isActive: row[Users.isActive],
                             addedDate: row[Users.addedDate],
                             userValidationVersion: row[Users.userValidationVersion],
                             passwordValidationVersion: row[Users.passwordValidationVersion],
                             cryptoVersion: row[Users.cryptoVersion] )
            }
        
        let validCredentialCount = userHistory.filter{$0.isActive}.count
        guard validCredentialCount <= 1 else {
            assert( false, "A User can have at most one active credential" )
            throw AuthError.internalError(message: "Database inconsistent, user: \(username) has \(validCredentialCount) valid credentials")
        }
        
        return userHistory
    }
}
