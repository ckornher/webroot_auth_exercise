//
//  SimplePasswordValidator.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

class SimplePasswordValidator : PasswordValidator {
    let passwordValidatorVersion = AuthServiceVersion.Password.v1
    
    let allRulesDescription = "Minimum of 8 characters, at least one number, at least one capital letter, at least one lowercase letter".localized
    
    /// A Real system would apply more sophisticated password rules and check against dictionaries
    func validate(password: String) throws {
        guard password.count >= 8 else {
            throw AuthIssueType.passwordNotStrongEnough.makeUserError("Password must be 8 or more characters long".localized)
        }
        
        let upperTest = NSPredicate(format:"SELF MATCHES %@", ".*[A-Z]+.*")
        guard upperTest.evaluate(with: password) else {
            throw AuthIssueType.passwordNotStrongEnough.makeUserError("Password must contain at least one uppercase letter".localized)
        }
        
        let lowerTest = NSPredicate(format:"SELF MATCHES %@", ".*[a-z]+.*")
        guard lowerTest.evaluate(with: password) else {
            throw AuthIssueType.passwordNotStrongEnough.makeUserError("Password must contain at least one lowercase letter".localized)
        }
        
        let numberTest = NSPredicate(format:"SELF MATCHES %@", ".*[0-9]+.*")
        guard numberTest.evaluate(with: password) else {
            throw AuthIssueType.passwordNotStrongEnough.makeUserError("Password must contain at least one number".localized)
        }
    }
}
