//
//  HomegrownAuthCrypto.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

/// I chose to create this homegrown class rather than use one of the existing libraries for a couple of reasons:
/// 1. To illustrate a simple, relatively secure password hashing system using random salts.
/// 2. Third-party crypto libraries should be carefully vetted before incorporating them into production code
class HomegrownAuthCrypto : PasswordCrypto {
    var cryptoVersion = AuthServiceVersion.Crypto.v1
    
    func hashPassword(_ password: String) throws -> (salt: String, hash: String) {
        let salt = try generateRandomString()
        let hash = hashMethod(password: password, salt: salt)
        return (salt: salt, hash: hash)
    }
    
    func authenticate(username: String, password: String, salt: String, hash: String) throws {
        // username is unused
        let newHash = hashMethod(password: password, salt: salt)
        guard newHash == hash else {
            throw AuthIssueType.authFailed.makeUserError()
        }
    }
    
    func hashMethod(password: String, salt: String) -> String {
        let saltedPassword = "\(password)\(salt)"
        return saltedPassword.sha1
    }
    
    // MARK: Internal
    
    /// Uses Apple's preferred method: "Generates an array of cryptographically secure random bytes."
    /// from: https://stackoverflow.com/questions/39820602/using-secrandomcopybytes-in-swift
    private func generateRandomString(length: Int=64) throws -> String {
        
        var keyData = Data(count: 32)
        let result = keyData.withUnsafeMutableBytes {
            SecRandomCopyBytes(kSecRandomDefault, 32, $0.baseAddress!)
        }
        
        guard result == errSecSuccess else {
            throw AuthError.internalError(message: "Problem generating random bytes")
        }
        
        return keyData.base64EncodedString()
    }
}
