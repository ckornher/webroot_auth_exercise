//
//  EmailValidator.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

/// Some systems may want to use emails, others not.
/// Emails are widely accepted usernames. Using emails for this exercise allows me to ignore issues around
/// guiding users to create unique usernames.
class EmailValidator : UsernameValidator {
    let userNameValidatorVersion = AuthServiceVersion.Username.email_v1
    
    /// "No email regex is 100% accurate", but this this is a pretty widely accepted version in Swift.
    /// Extensive testing of this method is out of scope for  an 8 hour expercise.
    /// - note: Taken from: https://stackoverflow.com/questions/25471114/how-to-validate-an-e-mail-address-in-swift
    /// and http://emailregex.com
    func validate(username: String) throws {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        if !emailPredicate.evaluate(with: username) {
            throw AuthIssueType.usernameInvalid.makeUserError()
        }
    }
}
