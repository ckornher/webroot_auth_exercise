//
//  SimpleHistoryChecker.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

class SimpleHistoryChecker : CredentialHistoryChecker {
    // Systems that expire passwords would have a method to return time to expiration. I hate those warnings, so I left it out :)
    // The password store supports such calculations, however
    
    // Not used in this example
    func checkNotExpired( credential: CredentialEntry) throws -> Bool {
        // A real system might check to see how old the password is or if it was created with an out-of-date alogrithm
        // or if it was subject to a data breach
        
        return true
    }
    
    func checkPasswordHistory(username: String, password: String, crypto: PasswordCrypto, store: AuthStore) throws {
        let history = try store.credentialHistory(username: username)
        for previous in history {
            // A more sophisticated test might look at the date added
            guard crypto.hashMethod(password: password, salt: previous.salt) != previous.hash else {
                throw AuthIssueType.passwordRepeted.makeUserError()
            }
        }
    }
    
    func checkWeakness(oldPassword: String, newPassword: String) throws {
        // A real system might compute the Levenshtein distance
    }
}
