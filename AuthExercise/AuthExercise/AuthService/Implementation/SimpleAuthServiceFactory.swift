//
//  TestAuthService.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

public func makeSimpleAuthService() -> AuthService {
    return AuthServiceImpl(usernameValidator: EmailValidator(),
                           passwordValidator: SimplePasswordValidator(),
                           authStore: SQLiteAuthStore(),
                           crypto: HomegrownAuthCrypto(),
                           historyChecker: SimpleHistoryChecker() )
}
