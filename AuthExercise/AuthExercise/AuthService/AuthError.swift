//
//  AuthError.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

/// The functionally different types of user errors. This avoids having to create a case for every possible error cause
enum AuthError : Error {
    case userError( AuthIssueType, String )       // Message will ne presented to the user and should be localized
    case internalError( message: String )         // Message will be logged
}

extension AuthError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .userError( _, let message):
            return message

        case .internalError(let message):
            return message                  // Note that this may not be localized, but these are messages are just for the log
        }
    }
}

/// User-visible and actionable issues. These are passed in AuthError.userError
/// - note: The raw values are only defaults. Components are free to make their own, specific messages
enum AuthIssueType : String {
    case authFailed = "No matching username/password found"
    case usernameInvalid = "You must enter a valid email"
    case newPasswordsDontMatch = "The new password entries are not the same"
    case passwordNotStrongEnough = "The password is not strong enough"
    case passwordRepeted = "This password was used before"
    
    func makeUserError(_ nonDefaultMessage: String?=nil) -> AuthError {
        let message = nonDefaultMessage ?? self.rawValue.localized
        
        return AuthError.userError(self, message)
    }
}

