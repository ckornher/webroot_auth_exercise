//
//  AuthService.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation

// Mark: AuthService Implementation (Thread Safe)
public protocol AuthService {
    var passwordRules: String { get }
    
    func start() throws
    func register(username: String, password1: String, password2: String) throws
    func authenticate(username: String, password: String) throws -> Bool
    func changePassword(username: String, oldPassword: String, newPassword1: String,  newPassword2: String) throws
}
