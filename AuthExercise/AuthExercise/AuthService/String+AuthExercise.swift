//
//  String+AuthExercise.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/6/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Foundation
import CommonCrypto

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    /// From: https://stackoverflow.com/questions/25761344/how-to-hash-nsstring-with-sha1-in-swift
    var sha1: String  {
        let data = Data(self.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0.baseAddress, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
}


