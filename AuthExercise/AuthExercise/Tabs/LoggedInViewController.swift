//
//  LoggedInViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class LoggedInViewController: BaseAuthViewController {
    @IBOutlet weak var usernameLabel: NSTextField!
    
    override func viewWillAppear() {
        super.viewWillAppear()
        usernameLabel?.stringValue = userAuthorization.authedUsername ?? ""
    }

    @IBAction func logoutButtonClicked(_ sender: Any) {
        userAuthorization.logout()
    }
}
