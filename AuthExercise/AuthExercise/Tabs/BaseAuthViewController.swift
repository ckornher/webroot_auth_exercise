//
//  BaseAuthViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class BaseAuthViewController: NSViewController {

    var userAuthorization: UserAuthorization!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    func alertUser(title: String, message: String, alertStyle: NSAlert.Style = .warning) {
        let alert = NSAlert()
        alert.messageText = title
        alert.informativeText = message
        alert.alertStyle = alertStyle
        alert.addButton(withTitle: "OK")
        alert.runModal()
    }
}
