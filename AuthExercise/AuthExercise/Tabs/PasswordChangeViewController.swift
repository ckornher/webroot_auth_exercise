//
//  PasswordChangeViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class PasswordChangeViewController: BaseAuthViewController {
    @IBOutlet weak var usernameLabel: NSTextField!

    @IBOutlet weak var oldPasswordField: NSSecureTextField!
    @IBOutlet weak var newPasswordField: NSSecureTextField!
    @IBOutlet weak var verifyNewPasswordField: NSSecureTextField!
    
    @IBOutlet weak var changeButton: NSButton!
    @IBOutlet weak var passwordRulesLabel: NSTextField!
 
    override func viewWillAppear() {
        super.viewWillAppear()
        usernameLabel.stringValue = userAuthorization.authedUsername ?? "Unknown User"
        passwordRulesLabel?.stringValue = userAuthorization.authService.passwordRules
        
        oldPasswordField.stringValue = ""
        newPasswordField.stringValue = ""
        verifyNewPasswordField.stringValue = ""
    }
    
    @IBAction func changeButtonClicked(_ sender: Any) {
        do {
            try userAuthorization.changePassword(username: usernameLabel.stringValue,
                                                 oldPassword: oldPasswordField.stringValue,
                                                 password: newPasswordField.stringValue,
                                                 passwordCopy: verifyNewPasswordField.stringValue)
        }
        catch UIError.any(let message) {
            alertUser(title: "Could Not Change Password".localized, message: message)
        }
        catch {
            alertUser(title: "Could Not Change Password".localized, message: userAuthorization.internalErrorMessage)
        }
        oldPasswordField.stringValue = ""
        newPasswordField.stringValue = ""
        verifyNewPasswordField.stringValue = ""
   }
}
