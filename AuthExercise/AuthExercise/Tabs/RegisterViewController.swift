//
//  RegisterViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/7/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class RegisterViewController: BaseAuthViewController {

    @IBOutlet weak var usernameField: NSTextField!
    
    
    @IBOutlet weak var passwordField: NSSecureTextField!
    
    @IBOutlet weak var verifyPasswordField: NSSecureTextField!
    
    @IBOutlet weak var registerButton: NSButton!
    
    @IBOutlet weak var passwordRulesField: NSTextField!
    
    override func viewWillAppear() {
        super.viewWillAppear()
        passwordRulesField?.stringValue = userAuthorization.authService.passwordRules
        
        usernameField.stringValue = ""
        passwordField.stringValue = ""
        verifyPasswordField.stringValue = ""
    }
        
    @IBAction func registerClicked(_ sender: Any) {
        do {
            try userAuthorization.register(username: usernameField.stringValue,
                                           password: passwordField.stringValue,
                                           passwordCopy: verifyPasswordField.stringValue)
            
            alertUser(title: "Registration Successful",
                      message: "You can now login",
                      alertStyle: .informational)

        }
        catch UIError.any(let message) {
            alertUser(title: "Login Failed".localized, message: message)
        }
        catch {
            alertUser(title: "Login Failed".localized, message: userAuthorization.internalErrorMessage)
        }
        passwordField.stringValue = ""
        verifyPasswordField.stringValue = ""
    }
}
