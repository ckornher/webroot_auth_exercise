//
//  LoginViewController.swift
//  AuthExercise
//
//  Created by Christopher Kornher on 5/5/19.
//  Copyright © 2019 Christopher Kornher. All rights reserved.
//

import Cocoa

class LoginViewController: BaseAuthViewController {

    @IBOutlet weak var usernameField: NSTextField!
    
    @IBOutlet weak var passwordField: NSSecureTextField!
    @IBOutlet weak var loginButton: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear() {
        super.viewWillAppear()
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        do {
            try userAuthorization.login(username: usernameField.stringValue,
                                        password: passwordField.stringValue)
            usernameField.stringValue = ""
        }
        catch UIError.any(let message) {
           alertUser(title: "Login Failed".localized, message: message)
        }
        catch {
            alertUser(title: "Login Failed".localized, message: userAuthorization.internalErrorMessage)
        }
        passwordField.stringValue = ""
    }
}

